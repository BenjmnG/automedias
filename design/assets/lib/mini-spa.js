const t = 750;
let onGoing = false;

(function () {
  'use strict';

  /**
   * Load content into page without a whole page reload
   * @param {string} href URL to route to
   * @param {boolean} pushState whether to call history.pushState or not
   */


  function clear(container){
    //container.remove();
    document.querySelector('main:not(.previous.asGhost)').remove();
    document.querySelector('main:not(.previous)').focus();
    setTimeout(function() {
      body.classList.remove('transition')
    }, t*.25);
  }


  function load(href, pushState) {
    const container = $('main');
    const xhr = new XMLHttpRequest();

    xhr.onload = function () {
      const d = xhr.responseXML,
            dTitle = d.title || '',
            dContainer = $('main:not(.previous)', d),
            dBody = $('body', d);

      if(document.title != dTitle){

        window.scrollTo(0,0)
        if(onGoing){
          console.log('A transition is already on going. Cuting it up')
          clear(container);
        }


        onGoing = true;
        body.classList.add('transition')
        let dataServe = dContainer.getAttribute('data-type'),
            dataPrev = container.getAttribute('data-type');
        body.setAttribute('data-serve', dataServe)
        body.setAttribute('data-prev', dataPrev)
        
        container.classList = 'previous';
        container.setAttribute('aria-hidden', true);        

        container.insertAdjacentHTML(
          'afterend',
          (dContainer && dContainer.outerHTML) || '');
        document.title = dTitle;

        

        if (pushState) {
          history.pushState({}, dTitle, href);
        }   

        if(dataServe == 'article' || dataServe == 'video' || dataServe == 'interview' || dataServe == 'film'){
          if(dataPrev == 'publications_list'){
            let actionClose = document.querySelector('main:not(.previous) .action-close')
            actionClose.href = '/publication'
          }
        } else if(dataServe == 'event'){
          if(dataPrev == 'events_list'){
            let actionClose = document.querySelector('main:not(.previous) .action-close')
            actionClose.href = '/event'
          }
        }

        // Call global js functions
        if(dataServe == 'index'){
          indexPageEvent()
        }else{
          allPagesButIndexEvent()
          if(dataServe == 'event'){
            eventPageEvent()
          }
        }
        
        //container.scrollIntoView({block: "start"});

        setTimeout( function() {
          // remove element and class
          clear(container)
          onGoing = false;
        }, t);
      } else {
        // same page, just scroll top
        //window.scrollTo(0,0)
      }
    };
    xhr.onerror = function () {
      // fallback to normal link behaviour
      document.location.href = href;
      return;
    };
    xhr.open('GET', href);
    xhr.responseType = 'document';
    xhr.send();
  }

  function $(sel, con) {
    return (con || document).querySelector(sel);
  }

  /**
   * Search for a parent anchor tag outside a clicked event target
   *
   * @param {HTMLElement} el the clicked event target.
   * @param {number} maxNests max number of levels to go up.
   * @returns the anchor tag or null
   */
  function findAnchorTag(el, maxNests = 3) {
    for (let i = maxNests; el && i > 0; --i, el = el.parentNode) {
      if (el.nodeName === 'A') {
        return el;
      }
    }
    return null;
  }

  window.addEventListener('click', function (evt) {
    let baseUrl = $('meta[name="x-base-url"]')?.getAttribute('content') || '/';
    const el = findAnchorTag(evt.target);
    const href = el?.getAttribute('href');
    if (el && href) {
      if (
        href.startsWith('#') ||
        el.getAttribute('target') === '_blank' ||
        /\.\w+$/.test(href)
      ) {
        // eleventy urls in this configuration do not have extensions like .html
        // if they have, or if target _blank is set, or they are a hash link,
        // then do nothing.
        return;
      }
      // if the URL starts with the base url, do the SPA handling
      if (href.startsWith(baseUrl)) {
        evt.preventDefault();
        load(href, true);
      }
    }
  });

  window.addEventListener('popstate', function (e) {
    load(document.location.pathname, false);
  });
})();