// https://pannellum.org/documentation/api/

document.querySelector('#panorama').classList.add('active');

const viewer = pannellum.viewer('pan360', {
    "type": "equirectangular",
    "panorama": "/assets/img/fresque.webp",
    "showControls": false,
    "autoLoad": true,
    "pitch": -3, // Pencher le regard initial vers le haut (5) ou le bas (-5)
    "yaw": -90, // Orientation de la fresque initial (en degrès)
    "hfov": 120, // revient au zoom initial. 1 pour rendu flatout

    // giro

    "hotSpotDebug": false,
    /*"hotSpots": [
        {
            "pitch": 14.1,
            "yaw": 1.5,
            "type": "info",
            "text": "Baltimore Museum of Art",
            "URL": "https://artbma.org/"
        },
        {
            "pitch": -9.4,
            "yaw": 222.6,
            "type": "info",
            "text": "Art Museum Drive"
        },
        {
            "pitch": -0.9,
            "yaw": 144.4,
            "type": "info",
            "text": "North Charles Street"
        }
    ]*/
});


console.log(viewer.isOrientationSupported())
console.log(viewer.isOrientationActive())
console.log(window.innerHeight, window.innerWidth)

// if a Mobile device
if(window.innerWidth < 1000 && window.innerHeight > window.innerWidth){
    viewer.setHfov(90)
}

// Giro
viewer.startOrientation()

window.addEventListener('touchend', () => {
    console.log('touched')
    viewer.startOrientation()
}, false);

// const viewer = pannellum.viewer('pan360', {
//         "type": "multires",
//         "showControls": false,
//         "autoLoad": true,
//         "pitch": 0, // Pencher le regard initial vers le haut (5) ou le bas (-5)
//         "yaw": -90, // Orientation de la fresque initial (en degrès)
//         "hfov": 120, // revient au zoom initial. 1 pour rendu flatout
//         "multiRes": {
//             "basePath": "/assets/img/output/",
//             "path": "/%l/%s%y_%x",
//             "fallbackPath": "/fallback/%s",
//             "extension": "jpg",
//             "tileResolution": 512,
//             "maxLevel": 3,
//             "cubeResolution": 1832
//         }
// });