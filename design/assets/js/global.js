let body = document.querySelector('body'),
  main = document.querySelector('main'),
  url = window.location.href,
  y = 0,
  hueStart,
  hue = 0, lightest = 70,
  autoMotion;

const isReduced = window.matchMedia(`(prefers-reduced-motion: reduce)`) === true || window.matchMedia(`(prefers-reduced-motion: reduce)`).matches === true;

//
// Function
//

function reset(){
  main.classList.remove('open');
}

// Function to sort Data
function SortItem(el) {

  if(document.querySelector("button.focus")){
    (document.querySelector("button.focus")).classList.remove("focus")
  }
  el.classList.add("focus")
  
  let attribute = el.getAttribute('data-value'),
    items = document.querySelectorAll("main.open .list > li"),
    itemArray = Array.from(items),
    list = document.querySelector("main.open .list");

  list.setAttribute("data-sort-by", attribute)

  function sorter(a,b) {
    // dirty
    let va, vb;
    if(attribute == 'date'){
      va = a.dataset.date;
      vb = b.dataset.date;
    }else if(attribute == 'title'){
      va = a.dataset.title;
      vb = b.dataset.title;
    }else if(attribute == 'author'){
      va = a.dataset.author;
      vb = b.dataset.author;
    }

    if(attribute == 'type'){
      if(va >vb) return -1;
      if(va < vb) return 1;
      return 0;
    } else {
      if(va < vb) return -1;
      if(va > vb) return 1;
      return 0;
    }
  }

  let sorted = itemArray.sort(sorter);

  sorted.forEach(e =>
    document.querySelector("main > section > .list").appendChild(e));
}

function resetHue(){
  console.log('resetHue')
  hueStart = Math.round(Math.random() * (360 - 0));
  body.style.setProperty('--hue', hueStart + hue);
  ajustLuminosity()
}

function updateLightest(){
  body.style.setProperty('--lightest', lightest + "%");
}

function hsl360(){
  // Check if Hue value is in acceptable range
  if(hueStart + hue > 360){
    hue = -hueStart;}
  else if (hueStart + hue < 0){
    hue = hueStart;
  }
}

// Slightly update luminosity for blue and violet colors
function ajustLuminosity(){
  let lmin  = 70,
      lmax  = 85,
      h     = 20,
      hmin  = 210 - hueStart,
      hmax  = 310 - hueStart;
  // if hue is in danger zone (blue / violet color can't provide sufisant contrast)
  if(hue >= hmin && hue <= hmax ){
    if(hue <= hmin + h ){
      // if in the bottom on his zone
      lightest = Math.round( lmin + ( (hue - hmin) / 2) )
    } else if( hue >= hmax - h){
      // if in the top on his zone
      lightest = Math.round( lmin + ( (hmax - hue) / 2) )
    } else{
      lightest = lmax
    }
    updateLightest()
  } else if( lightest != lmin){
    // if not in danger zone but luminosity isn't coherent, reset to minimal value
    lightest = lmin;
    updateLightest()
  }
}

function killAutoMotion(){
    if(autoMotion){
      clearInterval(autoMotion);
    }
}

function killScrollMotion(){
  window.removeEventListener("scroll", scrollColor)
}
function killMotion(){
  killAutoMotion()
  killScrollMotion()
}

function scrollColor(){
  const brake = 100;

  let _y = 1 + (window.scrollY || window.pageYOffset);
  _y = Math.round(_y / brake);

  if (y != _y){
    if (y < _y) {
      hue++
    } else if (y > _y) {
      hue--
    }
    y = _y;
    body.style.setProperty('--hue', hueStart + hue);
    hsl360();
    ajustLuminosity();
  }
}

 
function colorMotion(){ 
  // Brake : higher is slower
  if (!isReduced) {
    // clear any AutoMotion
    killAutoMotion()
    window.addEventListener('scroll', scrollColor)
  } 
}

function colorAutoMotion(t=150){
  if (!isReduced) {
    // clear any Scroll Motion
    killScrollMotion()
    autoMotion = setInterval(function() {
      hue++
      hsl360();
      ajustLuminosity();
      body.style.setProperty('--hue', hueStart + hue );
    }, t);    
  }
}

//
// Event Listener
//


function indexPageEvent(){
  resetHue()
  colorAutoMotion()
}


function allPagesButIndexEvent(){
  let actionClose = document.querySelector(".action-close"),
    actionColorMode = document.querySelector("main:not(.previous) .action-colorMode"),
    sections = document.querySelectorAll("section");

  resetHue()
  colorMotion()

  actionClose.addEventListener("click", () => {
   reset()
  }, false);

  if(actionColorMode){actionColorMode.classList.add('bite')}
  actionColorMode.addEventListener("click", () => {
   body.classList.toggle("darkMode");
  }, false);

  sections.forEach(function(section){
    let toggle = section.querySelector('section > h1:first-child')
    if(toggle){
      toggle.addEventListener("click", () => {
        section.classList.toggle("closed");
      }, false);
    }
  });

  // reset Share Button
  ShareButtons.update()

  printOnDemand()

}

function eventPageEvent(){
  let guestButtons = document.querySelectorAll("#guests button");

  guestButtons.forEach(function(guestButton){
    guestButton.addEventListener("click", () => {
      syncGuest(guestButton)
    }, false);
    guestButton.addEventListener("mouseover", () => {
      syncGuest(guestButton)
    }, false);
  });

  function syncGuest(guestButton){
    let n      = guestButton.getAttribute("data-item"),
      buttonShowed = document.querySelector("#guests ul.artefact li.show"),
      buttonShow  = guestButton.parentNode,
      bioShowed  = document.querySelector("#guests ul:not([aria-hidden='true']) li.show"),
      bioShow   = document.querySelector("#guests ul:not([aria-hidden='true']) li[data-item='"+ n +"']")

    if(buttonShowed){buttonShowed.classList.remove("show");}
    if(bioShowed){bioShowed.classList.remove("show");}
    buttonShow.classList.add("show");
    bioShow.classList.add("show");
  }
  
}

function printOnDemand(){
  if(url.includes('?print') ) {
    body.classList = 'print';
    main.classList = 'print';
    if(url.includes('&screen') ) {
          body.classList.add('forscreen');
          main.classList.add('forscreen');
    }
    if(main.getAttribute('data-printed-color')){
      body.style.setProperty('--hue', main.getAttribute('data-printed-color'));
      body.style.setProperty('--lightest', 60 + "%");
    }
   
    // load bespoke css
    // let link = document.createElement( "link" );
    //   link.href = window.location.origin + '/assets/css/print.css';
    //   link.type = "text/css";
    //   link.rel = "stylesheet";
    //   link.media = "screen";
    // document.getElementsByTagName( "head" )[0].appendChild( link );

    let scripts = ['/assets/lib/paged.js']

    // load cover fallback if needed
    if(document.querySelector('#coverFallback')){
      let lazy = document.querySelectorAll('.lazy');
      lazy.forEach( el => {
        let src   = el.getAttribute('data-src')
        el.setAttribute('src', src)
      })
    }
    
    // load pagedJs script
    scripts.forEach( function(path, i){
      let script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = window.location.origin + path; 
      document.getElementsByTagName('body')[0].appendChild(script);
    });

    killMotion()

  }
}

function killVideo(){
  var videos = document.querySelectorAll('.ltv-activated');
  videos.forEach( video => {
    video.classList.remove('ltv-activated')
    let iframe = video.querySelector('iframe')
    iframe.remove();
  });
}

//
// On init
//

body.classList.remove('noJs')
//body.setAttribute("data-color-set", Math.floor(Math.random() * 10 ) + 1)

if(body.getAttribute('data-serve') != 'index'){
  allPagesButIndexEvent()
  if(body.getAttribute('data-serve') == 'event'){
    eventPageEvent();
  }
} else{
  // if Index
  indexPageEvent()
}



