// cover
const { promises } = require('fs'),
      { join } = require('path'),
      { createCanvas } = require('@napi-rs/canvas');     


//
// Cover
//
const wrapText = function(ctx, text, x, y, maxWidth, lineHeight) {
    // First, split the words by spaces
    let words = text.split(' ');
    // Then we'll make a few variables to store info about our line
    let line = '';
    let testLine = '';
    // wordArray is what we'l' return, which will hold info on 
    // the line text, along with its x and y starting position
    let wordArray = [];
    // totalLineHeight will hold info on the line height
    let totalLineHeight = 0;

    // Next we iterate over each word
    for(var n = 0; n < words.length; n++) {
        // And test out its length
        testLine += `${words[n]} `;
        var metrics = ctx.measureText(testLine);
        var testWidth = metrics.width;
        // If it's too long, then we start a new line
        if (testWidth > maxWidth && n > 0) {
            wordArray.push([line, x, y]);
            y += lineHeight;
            totalLineHeight += lineHeight;
            line = `${words[n]} `;
            testLine = `${words[n]} `;
        }
        else {
            // Otherwise we only have one line!
            line += `${words[n]} `;
        }
        // Whenever all the words are done, we push whatever is left
        if(n === words.length - 1) {
            wordArray.push([line, x, y]);
        }
    }

    // And return the words in array, along with the total line height
    // which will be (totalLines - 1) * lineHeight
    return [ wordArray, totalLineHeight ];
}

module.exports = async function createCover(postTitle) {
  const canvas = createCanvas(1280, 768)
  const ctx = canvas.getContext('2d')
  ctx.fillStyle = '#FF3D60'
  ctx.fillRect(0, 0, 1280, 768);

  ctx.fillStyle = '#3d000b'
  ctx.font = '148px sans-serif';
  ctx.textAlign = "center";
  //let wrappedText = wrapText(ctx, postTitle, 50, 50, 1200, 300);
  //wrappedText[0].forEach(function(item) {
      // We will fill our text which is item[0] of our array, at coordinates [x, y]
      // x will be item[1] of our array
      // y will be item[2] of our array, minus the line height (wrappedText[1]), minus the height of the emoji (200px)
      //ctx.fillText(item[0], item[1], item[2] - wrappedText[1] - 100); // 200 is height of an emoji
  //})
  ctx.fillText(postTitle, 600, 350); // 200 is height of an emoji

  const pngData = await canvas.encode('png') // JPEG and WebP is also supported
  // encoding in libuv thread pool, non-blocking
  await promises.writeFile(join('./design/assets/social', encodeURIComponent(postTitle) + '.png'), pngData)
}
