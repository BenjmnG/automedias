//requires
const fs   = require('fs')

// check if type existe

// set type
const args  = process.argv.slice(2);
let type
if( args[0] == 'event'){ 
  type = 'event'
} else if( args[0] == 'article' || args[0] == 'interview' || args[0] == 'video' ){
  type = 'publication'
} else {
  console.log("\x1b[33m", 'Type d\'article non valide');
  return;
}

console.log("Working on a ", args[0])



// count existing item
let folder = type.charAt(0).toUpperCase() + type.slice(1),
    dir   = './contenu/' + folder;

fs.promises.readdir(dir, (err, files) => {
}).then( files => {
    let item_count = files.length
    createNew(item_count)
  }
)

function createNew(item_count){
  // Copy archetype
  let title

  if(args[1]){
    title = args[1]
  } else {
    title = 'REPLACE_TITLE'
  }
  let three_digits_item_count = ('000' + (item_count + 1)).substr(-3)
  let filename =  three_digits_item_count + 
                  '-' + args[0].substring(0,1).toUpperCase() + 
                  '-' + title;

  /*fs.copyFile('design/archetype/'+ type +'.md', filename + '.md', (err) => {
    //if (err) throw err;
    console.log( folder + '/' +  filename + '.md is create');
  });*/

  let data = fs.readFileSync('design/archetypes/'+ type +'.md',
              {encoding:'utf8', flag:'r'});
   
  // Display the file data
  let date = (new Date()).toISOString().split('T')[0];

  data =  data.replaceAll('2000-01-01', date)
              .replace('--REPLACE_TITLE--', title.replaceAll('_', ' '))
              .replace('--REPLACE_TYPE--', args[0])


  console.log(data)
  console.log('contenu/' + folder + '/' +  filename + '.md')
  fs.writeFile('contenu/' + folder + '/' +  filename + '.md', data, function (err) {
    if (err) throw err;
    console.log('File is created successfully.');
  });

}
