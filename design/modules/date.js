// Nunjuck date
let monthsEn = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
let monthsFr = ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"];

const nth = function(d) {
    if (d > 3 && d < 21) return 'th';
    switch (d % 10) {
        case 1:  return "st";
        case 2:  return "nd";
        case 3:  return "rd";
        default: return "th";
    }
}

function date(date){
  let en =   '<time lang="en" datetime="' + date
            + '"><span class="sMonth">' + monthsEn[date.getMonth()]  +' </span>'
            + '<span class="sDay">' + (date.getDate())  
            + '<sup>' + nth(date.getDate() + 1) + '</sup>' + '</span>'
            + '<span class="sComma">, </span>'
            + '<span class="sYear">' + date.getFullYear()  +'</span></time>';
  
  let fr =   '<time lang="fr" datetime="' + date  + '">'
            + '<span class="sDay">' + (date.getDate()) + '</span> ' 
            + '<span class="sMonth">' + monthsFr[date.getMonth()]  +' </span> '
            + '<span class="sYear">' + date.getFullYear()  +'</span></time>';

  return en + fr
}


function dateCompare(date, lang){

  date = Array.from(date)
  
  let options = {
    year: "numeric",
    month: "long",
    weekday: "long",
    hour: "numeric",
    minute: "numeric",
  },
  formatDateEn,
  formatDateFr;


  if(date.length == 1){
    formatDateEn = monthsEn[date[0].getMonth()] + ' ' 
        + date[0].getDate() 
        + '<sup>' + nth(date[0].getDate())  + '</sup>'
        + ', ' 
        + date[0].getFullYear()

    formatDateFr = date[0].getDate()  + ' ' 
        + monthsFr[date[0].getMonth()] + ' ' 
        + date[0].getFullYear()

  } else{

      let date1 = date[0]
      let date2 = date[date.length - 1]

      if( date1.getFullYear() == date2.getFullYear()){
        // if same year
        if( date1.getMonth() == date2.getMonth()){

          // 1 → 1/2/2022
          formatDateEn  = '<span class="sMonth">' + monthsEn[date1.getMonth()] + '</span> '
                        + '<span class="sDay">'   + date1.getDate()   + '</span>'
                        + '<sup>' + nth(date1.getDate()) + '</sup>'
                        + ' → ' 
                        + '<span class="sDay">'   + date2.getDate()   + '</span>'
                        + '<sup>' + nth(date2.getDate()) + '</sup>'
                        + ', ' 
                        + '<span class="sYear">'  +  date2.getFullYear() + '</span>'

          formatDateFr  = '<span class="sDay">'   + date1.getDate()   + '</span>'
                        + ' → ' 
                        + '<span class="sDay">'   + date2.getDate()   + '</span> '
                        + '<span class="sMonth">' + monthsFr[date2.getMonth()] + '</span> ' 
                        + '<span class="sYear">'  +  date2.getFullYear() + '</span>'

        } else {
          // 1/1 → 1/2/2022
          formatDateEn  = '<span class="sMonth">' + monthsEn[date1.getMonth()] + '</span> '
                        + '<span class="sDay">'   + date1.getDate()   + '</span>'
                        + '<sup>' + nth(date1.getDate()) + '</sup>'
                        + ' → ' 
                        + '<span class="sMonth">' + monthsEn[date2.getMonth()] + '</span> '
                        + '<span class="sDay">'   + date2.getDate()   + '</span>'
                        + '<sup>' + nth(date2.getDate()) + '</sup>'
                        + ', ' 
                        + '<span class="sYear">'  +  date2.getFullYear() + '</span>'

          formatDateFr  = '<span class="sDay">'   + date1.getDate()   + '</span> '
                        + '<span class="sMonth">' + monthsFr[date1.getMonth()] + '</span> '
                        + ' → ' 
                        + '<span class="sDay">'   + date2.getDate()   + '</span> '
                        + '<span class="sMonth">' + monthsFr[date2.getMonth()] + '</span> ' 
                        + '<span class="sYear">'  +  date2.getFullYear() + '</span>'

        }
      } else {        
        // 1/1/2021 → 1/1/2022
          formatDateEn  = '<span class="sMonth">' + monthsEn[date1.getMonth()] + '</span> ' 
                        + '<span class="sDay">'   + date1.getDate() + '</span>, '
                        + '<sup>' + nth(date1.getDate()) + '</sup>'
                        + '<span class="sYear">'  + date1.getFullYear() + '</span>' 
                        + ' → ' 
                        + '<span class="sMonth">' + monthsEn[date2.getMonth()] + '</span> ' 
                        + '<span class="sDay">'   + date2.getDate() + '</span>, '
                        + '<sup>' + nth(date1.getDate()) + '</sup>'
                        + '<span class="sYear">'  + date2.getFullYear() + '</span>' 


          formatDateFr =  '<span class="sDay">'   +   date1.getDate()           + '</span> ' 
                        + '<span class="sMonth">' + monthsFr[date1.getMonth()]  + '</span> ' 
                        + '<span class="sYear">'  +  date1.getFullYear() + '</span>'
                        + ' → ' 
                        + '<span class="sDay">'   +   date2.getDate()           + '</span> ' 
                        + '<span class="sMonth">' + monthsEn[date2.getMonth()]  + '</span> ' 
                        + '<span class="sYear">'  +  date2.getFullYear()        + '</span>'                     
      }
  }

  formatDateEn = '<time lang="en" datetime="'+date[0]+'">' + formatDateEn + '</time>'
  formatDateFr = '<time lang="fr" datetime="'+date[0]+'">' + formatDateFr + '</time>'

  if(lang == "fr"){
    return formatDateFr 
  } else if(lang == "en"){
    return formatDateEn 
  } else {
    return formatDateEn + formatDateFr 
  }
}

function time(time){
  var hours = (time / 60);
  var rhours = Math.floor(hours);
  var minutes = (hours - rhours) * 60;
  var rminutes = Math.round(minutes);
  rminutes = rminutes.toString().padStart(2, '0');
  return rhours + ":" + rminutes;
}

// is date is past
function dateIsPast(date) {
  const today = new Date()
  if(Array.isArray(date)){ date = date[date.length - 1] }
  return date < today;
}


module.exports = {date, dateCompare, time, dateIsPast}