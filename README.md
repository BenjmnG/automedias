# Automedias

Le site est un site statique généré par eleventy.

Les fichiers de travails du site sont synchronisé sur (un Gitlab)[https://gitlab.com/BenjmnG/automedias]
Le projet est hébergé directement sur Gitlab (cette solution gratuire et simple à maintenir restera viable tant que le site reste de taille modeste)

L'action d'éditer le site repose sur un triptique : 
Edition d'un fichier texte → Génération du site → Stockage des fichiers générés

En clair, une modification dans les fichiers textes induit la regénération des fichiers du site puis leurs transfert (automatique) vers le serveur hébergeant le site.


## Structure

Le projet est constitué de 3 dossiers
+ `Contenu` 👉 Le contenu du site en fichier texte
+ `Design` 👉 Les gabarits, fichiers de style, typographie
+ `Public` 👉 C'est le site générée, pret à être navigué 🏄

## Articles

La création et rédaction d'article se fait au sein du dossier Contenu. Il est possible de créer deux types de contenu : des événements et des publications. 

+ event
+ publication: 
	+ article
	+ video
	+ interview


## Action Rapide

+ Télécharger les mises à jours issues du Gitlab (_Repo_)
	`git pull`
+ Lancer une version local du site afin de prévisualiser les changements
	`npm run preview`
+ Sauvegarder tes derniers changements sur le Repo
	`npm run git`
+ Mettre à jour le site ( et sauvegarder par la même occasion)
	`npm run publish`


***

## Installation

*Note*

Durant les prochains chapitre, nous ferons la disctinction entre fichiers sources (le contenu et les gabarits) et les fichiers générés (la conjonction du contenu et des gabarits)

### Sans installation

Ils est possible de modifier le site sans installation, directement depuis l'endroit où sont héberger les fichiers sources du site [gitlab.com](https://gitlab.com). C'est de loin la solution la plus simple pour débuter l'édition du site Automedias. Toutefois cette méthode peut rapidement sembler fastidieuse et limitée.

+ Être connecté au site Gitlab et se rendre sur [la page du projet](https://gitlab.com/BenjmnG/automedias)
+ Ouvrir l'éditeur [*EDI Web*](https://gitlab.com/-/ide/project/BenjmnG/automedias/edit/master/-/)
+ Réaliser une modification sur le fichier désiré
+ Réaliser un *commit* sur la branche *master*
+ Sauf erreur de rédaction, le site se regénérera alors tout seul.

*note*

*commit* peut se traduire en français par la verbe commettre, faire une action tangible. Cela revient à *sauvgarder* les documents.

Nous entendrons régulièrement parlé de *branche*. Nous n'utiliserons que la branche principale *master* du projet.

*Attention*
Bien vérifier que le commit se réalise sur la branche principale. Non pas sur une autre branche.

### Avec Installation

+ Installer Atom
+ Installer le 


+ Installer dépendances `npm i`

## Rédaction, règles de base en Markdown 

| Élement | Syntaxe Markdown |
| ----------- | ----------- |
| Titres | `# H1`<br>`## H2`<br>`### H3` |
| Gras | `**bold text**` |
| Italique | `*italicized text*` |
| Citation | `> blockquote` |
| Liste ordonnée | `1. First item`<br>`2. Second item`<br>`3. Third item` |
| Liste non-ordonnée | `- First item`<br>`- Second item`<br> `- Third item` |
| Code | \``code`\` |
| Filet horizontal | `---` |
| Lien | `[title](https://www.example.com)` |
| Image | `![alt text](image.jpg)` |
| Note de bas de page 	|  `Here's a sentence with a footnote. [^1]`<br>`[^1]: This is the footnote.` | 
| ID de titre  | `### My Great Heading {#custom-id}` |
| Liste de définition |	`term`<br>`: definition`
| Barré |	`~~The world is flat.~~` |
| Indice | `H~2~O` |
| Exposant | `X^2^ ` |
| Surligné |	`I need to highlight these ==very important words==` |

### Paragraphes et sauts de ligne
Pour créer un paragraphe, laisser une ligne blanche entre deux lignes de texte :
> `Un premier paragraphe.` <br> <br> `Un deuxième paragraphe.`

Pour forcer un saut de ligne, saisir deux espaces en fin de ligne :
> `Un premier vers.  ` <br> `Un deuxième vers.`

### Styles de texte
Vous pouvez utiliser `_` ou `*` autour d'un mot pour le mettre en italique (deux pour le mettre en gras).
> `_italique_` s'affiche ainsi : _italique_  
> `**gras**` s'affiche ainsi : **gras**  
> `**_gras-italique_**` s'affiche ainsi : **_gras-italique_**  
> `~~barré~~` s'affiche ainsi : ~~barré~~  

### Liens
On peut créer un lien en mettant le texte cliquable entre crochets et l’URL associée entre parenthèses : 

> `Un [lien](https://automedias.org)` s’affiche ainsi : Un [lien](https://automedias.org)

### Images

Pour insérer une image, on peut utiliser la syntaxe Markdow native :
> `![Texte alternatif](url/de_limage.jpg)`

La syntaxe spécifique (non markdown) `(figure: fichier.jpg)` est plus appropriée à la structuration des documents et mémoires (notamment du fait de la possibilité de légendes).

### Citations
Des citations peuvent être créées grâce au signe `>` :

> `> Le texte de la citation !`

### Titres
Les titres et intertitres peuvent être crées grâce à `#` répété une ou plusieurs fois en début de ligne :
> `# Titre de niveau 1`  
> `## Titre de niveau 2`  
> `### Titre de niveau 3`

Dans le contexte de ces documents, on évitera le niveau de titre 1, réservé au titre du document.

### Listes

Des listes ordonnées et non-ordonnées peuvent être créées grâce à la syntaxe suivante :
> `1. élément`  
> `2. élément`  
> `3. élément`

Ou
> `* élément`  
> `* élément`  
> `* élément`

### Ajouter des classes

Il est possible d'ajouter des *class* à certains éléments.
Une *class* est une étiquette qui permettra, si elle a été renseigné en amont, d'associer une mise en forme à l'élément.

Pour intégrer une class, celle-ci doit être à la fin de l'élément à styliser et respecter une syntaxe :

	Bonjour {.itw}

Ici "Bonjour" sera stylisé comme une interview

Voici les *class* existante :

| class 			| fonction 		|
| --------- 	| ----------- |
| .itw 				| marquer une interview |
| .link .l 		| stylise un lien avec plus de poid |
| .link .xxl	| stylise un lien comme un large boutton |
| .link .mail	| marquer un lien vers une adresse mail |
| .demi				| limiter la taille du bloc de texte à 50%|
| .color			| permet à une image de s'afficher en couleur |
| .minor			| permet à texte de s'afficher plus petit |
| .duo				| permet à texte de s'afficher sur deux colonnes |
| .sr-only		| permet de rendre un élément invisible sauf pour une liseuse d'écran |


### Aller plus loin

Lire la documentation sur [markdownguide.org](https://www.markdownguide.org/) et [MarkdownIt](https://markdown-it.github.io/) (cette librairie ajoute des éléments utiles : abbréviations, notes de bas de page, listes de définition, tableaux, attributs `class` et `id` pour les éléments…). 


## Créer un post

*en bref*

+ ` npm run create {TYPE} {NOM}` 
+ Modifier la page nouvellment créée

### Structure d'une fiche généralité

Chaque fiche article est divisée en deux partie:  un avant-propos (*frontmatter*) et une partie contenu à proprement parler.

L'avant propos est une section débutant et se concluant par trois tiret `---`. C'est ici que sont renseigné les métadonnée de la page tel que sont titre, son auteur ou plus largement le programme de la journée.

#### Avant-propos

Cette section respecte un language nommée *yaml* et peut se résumer à une succesion d'entrée et de valeur, séparé par deux points

`title: Le titre de l'article`

On retrouvera par moment des entrées de type liste :

`authors: 
	- Igor
	- Cemil
`

Par moment des imbrications :

```
cover: 
  fallback: image.jpg
  credit: blabla
```


*Attention* 
Une imbrication se signifie toujours par un retrait de deux expaces.

L'entrée parent ne contient aucune autre valeur que la ligne descendante

```
Maison:
  Chambre: "La chambre se situe dans la maison"
```

Sur ce même principe, nous réaliserons par moment des entrées de type listes imbriquée dans d'autres entrées :

```
Maison:
  - Chambre_1: "La chambre 1 se situe dans la maison"
  - Chambre_2: "La chambre 2 se situe aussi dans la maison"
```
```
author_contact:
  - type: mail
    link: igor@automedias.org
  - type: mail
    link: cemil@automedias.org
```

Enfin, si la valeur rédigée est trop longue ou complexe, il est conseillée de l'inclure entre deux guillemets anglais

`description:  "blabla
				blablabla
				bla bla bla: Blah !! "`

#### Contenu

Il se rédige après l'avant propos (après les seconds `---`) et en *markdown* (voir la section Markdown).

***

Ces principes de bases étant énnoncés, passons aux choses sérieuses.

***

### Ajouter une section annexe à une page événement ou publication

Rien de plus simple, il suffit d'inscrire le texte suivant dans l'avant-propos et de le compléter
```
aside:
  - title: Titre de la section
    content: "Contenu texte de la section. Longueur illimitée"
```

### Ajouter des métadonnée (toutes pages)

Il est possible de renseigner la langue de l'article. Le choix se fait entre français (fr) ou anglais (en)

`language: fr`

title: Pour une fabrique populaire de l’information à l’époque de la post&#8209;vérité
#### titre

*note* : Cette information est de celles déjà renseignées si l'on passe par la commande de création automatique d'article

`title: Titre de l'article`

#### Sous-titre

`subtitle: Sous-titre`

#### Etiquette

Le drapeau peut aussi être un lien (voir Markdown / Rédiger un lien)
`flag: populisme`

#### Rendre l'article invisible

`draft: false`

#### Definir ungabarit de mise en page

Pour un événement la valeur sera `event`. Pour une publication, la valeur sera logiquement `publication`

Dans le cadre d'une publication, il convient de définir le sous-type de mise en page :

+ article
+ video
+ interview

Exemple : 

```
layout: publication
type: article
```

#### Langue préférée

`language: fr`

#### Auteur.rice

```
author: 
  - Igor Galligo
  - Ludovic Duhem
  - Édouard Bouté
```

#### Description à l'attention des moteurs de recherche

`description: This is a post on ...`

#### date

*note* : Cette information est de celles déjà renseignées si l'on passe par la commande de création automatique d'article.

*note* : Cette valeur influence le classement de l'article dans la liste - les plus récent en premier

`date: 2022-07-13`

#### Etablir des liens avec des plublications internes

Cette entrée permet d'établir des références avec d'autres pages interne en fin de page.

```
linked_publications:
  - 002
  - 003
  - 004
  - 005
  - 006
```

### Ajouter des métadonnée (Page évènement)

```
# cover fallback
cover: 
  fallback: 
  credit: 
```

#### Couleur préférée pour la version imprimable
```
printed_color: 60
```

#### Ajouter un teasers

```
teasers:
  - title : REPLACE TEASER NAME
    plateform: vimeo
    id : 717220173 
```

#### Ajouter des infos pratiques

```
practical_info: 
  - title: Venir sur place
    description: "REPLACE TEXT"
```

#### Ajouter un lien d'inscription

```
inscription: 
  method: inscription
  howto: 
```

### Ajouter des métadonnée (Pages publication)


#### Bibliographie de l'article

La bibliographie est une simple liste. Chaque entrée est introduit par un retrait (deux espaces) et un tiret.
Il est possible de segmenter la bibliographie par catégorie. Il convient alors de renseigner un titre (- title: MON TITRE). L'ensemble des références placer à la ligne et respectant un retrait supplémentaire (deux espaces) seront inclut dans ce sous-groupe
Si les références contiennent des doubles points (ou colons), il convient remplacer ce symbole par l'expression &#58; ou de placer la référence entre "guillemet" pour ne pas venir en s'interpoler avec la syntaxe *yaml*. 

```
bibliography:
      -  Arquembourg, J. (2011). L'événement et les médias. Les récits médiatiques des tsunamis et les débats publics (1755-2004). Paris, Éd. des Archives contemporaines.

      -  Bourdieu, P. (1992(1998). Les règles de l’art. Genèse et structure du champ littéraire. Paris, Ed. du Seuil.
```

Ou

```
bibliography: 
  - title: Titre de la section
    references:
      -  Arquembourg, J. (2011). L'événement et les médias. Les récits médiatiques des tsunamis et les débats publics (1755-2004). Paris, Éd. des Archives contemporaines.

      -  Bourdieu, P. (1992(1998). Les règles de l’art. Genèse et structure du champ littéraire. Paris, Ed. du Seuil.
```

***

## Actions rapide (via un terminal de commande)

### prévisualiser le site *en local* sur son ordinateur

`npm run preview`

### Mettre le site à jour

`npm run publish`

### Créer un nouveau post

Créer un nouveau post se fait dans un terminal avec la commande ` npm run create {TYPE} {NOM}`

####  Les types

Il est possible de créer les types de pages suivantes :

+ Un événement en mentionnant `create event`
+ Une publication de type
+ +  article en mentionnant `create article`
+ +  interview en mentionnant `create interview`
+ +  video en mentionnant `create video`


Par exemple, pour créer un article, on utilisera la commande ` npm run create article `

#### Le nom

Il est aussi possible (et conseillé) de mentionner des à présent le titre de l'article ou de l'évenement.
Celui-ci devra ici s'écrire en remplaçant les espaces par des *_*. (Exemple: *Le_nom_de_l'article*)

Par exemple, pour créer un article nommé *Il ne faut pas désespérer le Metaverse* on commandera :
`npm run create article Il_ne_faut_pas_désespérer_le_Metaverse`

#### Conséquence

Une fiche article sera nouvellement créer dans le dossier contenu (*event* ou *publication* suivant le type d'article demandé). Cette fiche sera remplie de plusieurs champs vide (une sorte d'archétype de la page parfaite mais sans contenu). Il conviendra de les remplir en fonction des besoins.
Les champs pré-inscrit ne sont pas exhaustif. Il sera possible de les compléter en prennant appuie sur la section *écrire une fiche article* de ce tutoriel.

A noter, qu'il est aussi possible de créer une fiche article manuellement directement dans le dossier avec un editeur de texte comme *Atom*. Pour cela on enregistrera dans le dossier `publication` une fiche markdown nommée `008-A-Il_ne_faut_pas_désespérer_le_Metaverse.md`.

Les trois premiers chiffre correspondent au numéro de l'article (ici le 8e), la lettre `A` au type de publication (`V` pour une vidéo).
A la charge du rédacteur de venir rédiger les champs de la fiche article, ou à les copier depuis un article déja existant. 

