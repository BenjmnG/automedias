const Image           = require('@11ty/eleventy-img'),
      htmlmin         = require("html-minifier"),
      markdownItAttrs = require('markdown-it-attrs'),
      markdownItFn    = require('markdown-it-footnote'),
      markdownIt      = require('markdown-it')({html: true,linkify: false,typographer: true}),
      yaml            = require("js-yaml"),
      fm              = require('yaml-front-matter'),
      typo            = require('typo'),
      {date, dateCompare, time, dateIsPast} 
                      = require('date')

// //
// // // Shortcode function
// // // //
async function imageShortcode(src, alt = "", sizes="1280px") {
  
  let metadata = await Image('./contenu/media/'+src, {
    widths: [1280],
    formats: ["webp", "jpg"],
    outputDir: "./public/media/couverture",
    urlPath: "../media/couverture",
    filenameFormat: function (id, src, width, format, options) {
      const extension = path.extname(src);
      const name = path.basename(src, extension);
      return `${name}-${width}.${format}`;
    }    
  });

  let imageAttributes = {
    alt,
    sizes,
    loading: "lazy",
    decoding: "async"
  };
  return Image.generateHTML(metadata, imageAttributes, {
    whitespaceMode: "inline"
  });
}

async function videoShortcode(src, alt = ""){
  let file_name = src.split("/").pop()
  return '<video muted loop controls autoplay title="'+(alt)+'"><source src="./media/'+file_name+'" type="video/webm"><source src="./media/'+file_name+'.mp4" type="video/mp4"></video>'
}

//
// Markdown
//
const markdownLibrary = markdownIt.use(markdownItAttrs).use(markdownItFn)

markdownLibrary.renderer.rules.footnote_block_open = () => (
  '<section class="footnotes">\n' +
  '<ol class="footnotes-list">\n'
);

markdownLibrary.renderer.rules.footnote_caption = (tokens, idx) => {
  let n = Number(tokens[idx].meta.id + 1).toString();

  if (tokens[idx].meta.subId > 0) {
    n += ":" + tokens[idx].meta.subId;
  }

  return n;
};

// Open link in new tab
var defaultRender = markdownLibrary.renderer.rules.link_open || function(tokens, idx, options, env, self) {
  return self.renderToken(tokens, idx, options);
};
markdownLibrary.renderer.rules.link_open = function (tokens, idx, options, env, self) {
  // If you are sure other plugins can't add `target` - drop check below
  var aIndex = tokens[idx].attrIndex('target');

  if (aIndex < 0) {
    tokens[idx].attrPush(['target', '_blank']); // add new attribute
  } else {
    tokens[idx].attrs[aIndex][1] = '_blank';    // replace value of existing attr
  }

  // pass token to default renderer.
  return defaultRender(tokens, idx, options, env, self);
};

//
// html minify
//
function htmlMinify(content, outputPath) {
    // Eleventy 1.0+: use this.inputPath and this.outputPath instead
    if( outputPath && outputPath.endsWith(".html") ) {
      let minified = htmlmin.minify(content, {
        useShortDoctype: true,
        removeComments: true,
        collapseWhitespace: true
      });
      return minified;
    }
    return content;
  }

// shuffle array
function shuffle(values) {
  return values.sort((a, b) => 0.5 - Math.random());
}

// clear double value
function uniq(a) {
    a = a.flat()
    return a.sort().filter(function(item, pos, ary) {
        return !pos || item != ary[pos - 1];
    });
}

// Time to read
function readingTime(text, lang="fr"){

    let regFn = new RegExp(/<section class=\"footnotes\".*?<\/section>/);
    text = ( text.replace(/\n/g, '') ).replace(regFn, '')

    // get entire post content element
    let wordCount = `${text}`.match(/\b[-?(\w+)?]+\b/gi).length;
    //calculate time in munites based on average reading time
    let timeInMinutes = (wordCount / 300)
    //validation as we don't want it to show 0 if time is under 30 seconds
    let output;
    if(timeInMinutes <= 0.5) {
      output = 1;
    } else {
      //round to nearest minute
      output = Math.round(timeInMinutes);
    }
    if(output > 1){
      if(lang =="en"){
        return `${output}` + ' minutes reading';
      } else{
        return `${output}` + ' minutes de lectures';
      }
    } else{
      return "";
    }
  }




// //
// // // 11ty configuration
// // // //

module.exports = config => {

  config.addFilter("markdown", content => markdownLibrary.render(content) );
  config.addFilter("typo", content => typo(content) );
  config.setLibrary("md", markdownLibrary);
  config.addFilter("debug", (content) => console.log(content));
  config.addFilter('shuffle', shuffle)
  config.addFilter('date', date);
  config.addFilter('dateCompare', (date, lang) => dateCompare(date, lang));
  config.addFilter('isArr', something => Array.isArray(something));
  config.addFilter('isObj', something => _.isPlainObject(something))
  config.addFilter('uniq', uniq)
  config.addFilter('time', time)
  config.addFilter('isInThePast', dateIsPast)
  config.addFilter("head", (array, n) => { if (n < 0) { return array.slice(n); } return array.slice(0, n); });
  config.addFilter("escaP", content => content.replace('<p>', '').replace('</p>', ''));
  
  config.addShortcode('dateCompare', (date, lang) => dateCompare(date, lang));
  config.addShortcode('readingTime', (content, lang) => readingTime(content, lang));

  config.addDataExtension("yaml", contents => yaml.load(contents));
  
  config.addNunjucksAsyncShortcode("image", imageShortcode);
  config.addNunjucksAsyncShortcode("video", videoShortcode);


  config.addCollection("publications", function(collection) {
      const now = new Date();
      const publishedPosts = (post) => !post.data.draft; // [1]
      let publications = collection.getFilteredByGlob("contenu/Publication/*.md").filter(publishedPosts);

      console.log(process.env.ELEVENTY_ENV)
      /*if(process.env.ELEVENTY_ENV == 'development'){
        publications.forEach( publication => createCover(publication.data.title) );
      }*/
      return publications;
  });

  config.addCollection("events", function(collection) {
      let events = collection.getFilteredByGlob("contenu/Event/*.md")
      return events;
  });

  config.setDynamicPermalinks(true);
  permalink: (data) => {
    let cat = (data.page.filePathStem).match(/([^\/]*)\/*$/)[1],
        lang = data.page.language ? data.page.language : "fr"
    return lang + '/' + cat + '/' + fileSlug + '.html';
  }

  // Sass
  config.addWatchTarget("./design/assets/scss/");
  // pass through !
  config.addPassthroughCopy({'./design/assets/font/' : 'assets/font'});
  config.addPassthroughCopy({'./design/assets/svg/' : 'assets/svg'});
  config.addPassthroughCopy({'./design/assets/img/' : 'assets/img'});
  config.addPassthroughCopy({'./design/assets/js/' : 'assets/js'});
  config.addPassthroughCopy({'./design/assets/lib/' : 'assets/lib'});
  config.addPassthroughCopy({'./design/assets/css/' : 'assets/css'});
  config.addPassthroughCopy({'./design/assets/social/' : 'assets/social/'});
  config.addPassthroughCopy({'./contenu/_media/' : 'assets/media/'});
  config.addPassthroughCopy({'./design/assets/root/' : '/'});
  //config.addPassthroughCopy({'./design/assets/*.htaccess' : '/'});

  config.addTransform("htmlmin", htmlMinify);

  return {
    markdownTemplateEngine: 'njk',
    dataTemplateEngine: 'njk',
    htmlTemplateEngine: 'njk',
    dir: {
      input: './contenu/',
      data: './_data/',
      includes: '../design/layouts',
      output: 'public'
    }

  };
};

