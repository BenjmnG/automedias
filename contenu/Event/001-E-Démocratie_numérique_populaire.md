---
title: Démocratie Numérique Populaire
subtitle: 
permalink: /event/{{title | slugify }}/

date: 2020-03-07
event_date: 
  - 2020-03-07

place: Maison des Sciences de l’Homme Paris-Nord
organizers: 
  - Igor Galligo
  - Cléo Collomb et Natasha (Aide à la coordination)
  - Max Mollon (Aide à la conception)

flag: Archive

layout: event

# Langue préférée: fr or en
language: fr

#
# Inscription
#
inscription: 
  method: inscription
  howto : "L'évènement est ouvert à toutes et tous. Inscription obligatoire. 

    Pour tous renseignements complémentaires : democratienumeriquepopulaire@protonmail.com"
  link: https://www.helloasso.com/associations/noodesign/evenements/democratie-numerique-populaire

#
# Retranscription vidéo
#

videos:
  - title : Automédias et médias critiques
    plateform: vimeo
    id : 454834339
  - title : Réseaux sociaux et expression démocratique
    plateform: vimeo
    id : 456985650
  - title : Les plateformes gilets jaunes - organisation et délibération
    plateform: vimeo
    id : 458926365

#
# Aside
#
aside:
  - title: Ressources
    content: "
      ![Affiche de l'evenement](/../../assets/media/events/001-poster-600.webp){.color loading=lazy}
      { .demi }

      - [Affiche de l'evenement](/../../assets/media/events/001-poster.pdf){.link .l}

      - [Programme de l'evenement](/../../assets/media/events/001-programme.pdf){.link .l}

      { .demi }
    "


#
# Programme
#

programme:

  # Jour 1
  - date: 2020-03-07
    title: Colloque **Démocratie Numérique Populaire**
    schedule:
      - session: Ouverture
        format: ouverture
        description: Accueil des participants
        start_time: 9:00
        end_time: 9:20

      - session: Ouverture
        format: 
        description: Ouverture 
        start_time: 9:20
        end_time: 9:30

      - session: Médiation & Médiatisation
        format: 
        description:
        moderator: Yves Citton
        guests: 
          - Cemil Sanli
          - Jude Mas
          - Julien Lepeigneul
          - Kévin Drobczynski
          - Lucas Remak
          - Natasha
          - Yohan Pavec
          - Aude Lancelin
          - Didier Maisto
          - Hervé Kempf
          - Stéphane Alliés
          - Dominique Cardon

        start_time: 9:30
        end_time: 11:00

      - session: Expression & expressivité
        format: 
        description:
        moderator: Igor Galligo
        guests: 
          - Faouzi Lellouche
          - Maxime Nicolle
          - Philippe le Hérisse
          - Priscillia Ludosky
          - Benjamin Coriat
          - Dominique Cardon
          - Dominique Pasquier
          - Luc Gwiazdzinski
          - Victor Petit

        start_time: 11:30
        end_time: 12:30

      #

      - break: true
        start_time: 12:30
        end_time: 14:15

      #

      - session: Organisation & délibération
        format: 
        description:
        moderator: Clément Mabi
        guests: 
          - David Prost
          - Didier Fradin
          - Florence
          - Hakim Lowe
          - Michael Gas
          - Nikos Bonnieure
          - Bernard Stiegler
          - Caterina Froio
          - Louis Ollagnon
          - Nicole Alix
          - Pascal Marchand
          - Pierre-Yves Gosset
          - Stephane Crozat
          - Vincent Ventresque
          - Laurent Jeanpierre
        start_time: 14:15
        end_time: 15:45

      - session: Duplex avec l'ADA 5 Toulouse 
        format: 
        description: Discussion entre l’assemblée de Démocratie Numérique Populaire en duplex avec l’Assemblée des Assemblées Gilets Jaunes n°5, à Toulouse
        start_time: 15:45
        end_time: 17:45

      - session: Ateliers collectifs avec le public
        format: 
        description:
        moderator: 
        workshop:
          - Médialab - Souveraineté et territoire
          - Médialab - Le Facebook Live
          - Médialab - Outils numériques de réflexivité sur les Gilets Jaunes
          - EnCommuns
          - Framasoft
          - FFDN
          - Assemblée Virtuelle
          - La Ligue Citoyenne
          - Objectif RIC
          - Culture RIC
          - RiseFor
          - Le Vrai Débat
          - Ateliers libres sur proposition de Gilets Jaunes 
        start_time: 14:15
        end_time: 17:45

      - session: Conclusion
        format: 
        description: Restitution des résultats des ateliers et des discussions avec l'ADA 5
        start_time: 18:00
        end_time: 19:00

      - session: Conclusion
        format: 
        description: Conclusion générale de la journée
        start_time: 19:00
        end_time: 19:30

#
# Partenaires
# Laisser "logo vide" ou l'effacer si pas de logo
#

partners:
  - name: UTC Costech
    category: Partenaires scientifiques et financiers
    link: http://www.costech.utc.fr/
  - name: En Commun
    category: Partenaires scientifiques et financiers
  - name: Science Po Médialab
    category: Partenaires scientifiques et financiers
  - name: CFE CGC
    category: Partenaires scientifiques et financiers
  - name: Quartier géneral
    category: Partenaires médias
  - name: Reporterre
    category: Partenaires médias
  - name: Mediapart
    category: Partenaires médias
  - name: Sud Radio
    category: Partenaires médias
  - name: Revol
    category: Automédias citoyens
  - name: Rogue
    category: Automédias citoyens 
  - name: Bang Bang
    category: Automédias citoyens 
  - name: La Mule du Pape
    category: Automédias citoyens 
  - name: Le canard réfractaire
    category: Automédias citoyens 
  - name: Cemil chose à te dire
    category: Automédias citoyens 
  - name: La ligne jaune
    category:  Associations Gilets Jaunes
  - name:  Culture RIC
    category:  Associations Gilets Jaunes
  - name: Le vrai débat
    category: Associations Gilets Jaunes
  - name: La Ligue citoyenne
    category: Associations Gilets Jaunes
  - name: Objectif RIC
    category: Associations Gilets Jaunes
  - name: FFDN
    category: Associations numériques citoyennes
  - name: Noödesign
    category: Associations numériques citoyennes
  - name: Cartodébat
    category: Associations numériques citoyennes
  - name: Framasoft
    category: Associations numériques citoyennes
  - name: \#RiseFor
    category: Partenaires universitaires
  - name:  Artec
    category: Partenaires universitaires
  - name: Triangle
    category: Partenaires universitaires
  - name: Lerass
    category: Partenaires universitaires
  - name: Labex COMOD
    category: Partenaires universitaires
  - name: Maison des Sciences et de l'Homme - Paris-Nord
    category: Partenaires universitaires


---

Loin des clichés médiatiques, il faut attester que le mouvement des Gilets Jaunes s’est différencié des luttes politiques françaises antérieures par de nombreux usages, pratiques et projets technologiques, médiatiques et numériques.

Les Gilets Jaunes ont habité les réseaux sociaux numériques (Facebook, Instagram, Whatsapp, Telegram, Mastodon, etc.) pour communiquer, se mobiliser, se coordonner, se regrouper. Certains ont fait corps avec de nouvelles technologies audiovisuelles (caméras embarquées, GoPro, smartphones, etc.) pour informer ou documenter des aspects méconnus ou des expériences  vécues au sein du mouvement social. D’autres plus célèbres se sont réappropriés des dispositifs tels que le Facebook 
Live pour créer de nouvelles manières interactives de faire communauté.
Au-delà de ces appropriations, les Gilets Jaunes sont aussi devenus des concepteurs de plateformes numériques pour développer de nouveaux outils technologiques d’organisation et de délibération.

Les Gilets Jaunes ne se sont donc pas seulement retrouvés sur les ronds-points de France ou venus manifester sur les Champs-Elysées, ils ont en plus imaginé et produit des conceptions numériques de 
la politique, fondées sur leurs cultures et leurs enjeux, afin de développer de nouvelles puissances et expériences politiques.

C’est cette nouvelle Démocratie Numérique Populaire que nous souhaitons mettre en discussion le 7 mars entre des chercheurs, des ingénieurs, des designers numériques et bien sûr de nombreux Gilets 
Jaunes qui viendront de Paris, de région parisienne et de province.
Nous espérons que les usages et productions numériques des Gilets Jaunes qui seront présentés puissent être nourris par la diversité des intervenants invités qui prendront part aux débats. 
L’après-midi, des ateliers communs (intervenants + public) seront organisés pour approfondir ces discussions avec l’ensemble des personnes présentes à l’évènement. Ces ateliers seront animés par 
les institutions et associations partenaires, citoyennes, Gilets Jaunes ou spécialisées sur le numérique.
L’évènement sera filmé et retransmis en direct par l’équipe de la chaîne Quartier Général.

Nous serons également en duplex avec l’Assemblée des Assemblées n°5 des Gilets Jaunes, qui aura lieu à Toulouse, le même jour.

En espérant vous voir nombreux !