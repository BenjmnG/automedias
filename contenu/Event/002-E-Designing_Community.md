---
title: Designing Community
subtitle:
permalink: /event/{{title | slugify }}/

date: 2019-04-19
event_date: 
  - 2019-04-19
  - 2019-04-20

place: Espace Niemeyer, Paris
organizers: 
  - Igor Galligo
  - Cléo Collomb et Natasha (Aide à la coordination)
  - Max Mollon (Aide à la conception)

flag: Archive


layout: event

# Langue préférée: fr or en
language: fr

#
# Inscription
#
inscription: 
  method: inscription
  howto : "L'évènement est ouvert à toutes et tous. Inscription obligatoire. 

    Pour tous renseignements complémentaires : democratienumeriquepopulaire@protonmail.com"
  link: https://www.helloasso.com/associations/noodesign/evenements/democratie-numerique-populaire

#
# teasers
#
teasers:
  - title : Designing community - teaser
    plateform: vimeo
    id : 327944021 
#
# Vidéo
#
videos:
  - title : Noodesign
    plateform: vimeo
    id : 623109512
  - title : Remember
    plateform: vimeo
    id : 336355108 

#
# Aside
#
aside:
  - title: Interventions
    content: "
    # Dire « nous » aujourd’hui
    MARIELLE MACÉ

    On éprouve aujourd’hui un véritable appétit pour le pronom « nous », sa force et sa chaleur. Je voudrais examiner, en littéraire, les enjeux de cet appétit. Il s’agit en particulier de souligner que la question du « nous » n’est pas celle des identités ou des appartenances. « Nous » ne désigne pas un agrégat de personnes, mais un sujet collectif. Et c’est politiquement assez différent : il ne s’agit pas de dénombrer des petits groupes dans un « tout », mais de définir des façons multiples de se relier, de former un pluriel, et pas n’importe quel type de pluriel : un pluriel suffisamment soudé pour qu’il puisse s’énoncer, un pluriel défini par la densité des liens qui le composent.


    # Vivre l’époque
    PIERRE-DAMIEN HUYGHE

    La communauté, c’est du commun lié, noué, attaché à un nom, à une histoire, voire à un mythe. Il faut commencer par là : dire que la communauté, ce n’est pas le commun du commun, lequel se trouve répandu – éparpillé, disséminé – au sein d’un monde où il grouille d’activités diverses et innombrables. Le commun, ce sont les affairements et les bricolages du plus grand nombre avec les capacités techniques de l’époque, c’est cette époque telle que vécue. Qu’est-ce que ce vécu ? Qu’est-ce que vivre une époque ? Et même, pour être plus précis ou plus circonstancié, qu’est-ce que vivre cette époque, la nôtre, qui dispose (de) nombre d’appareils d’enregistrement, de moteurs de toutes sortes et de capacités de gestion statistique à leur tour produites par des appareillages et des motorisations à la profondeur historique restreinte ? Dans quelle mesure ces dispositions sont-elles, pour le commun, organiques ? Pareilles questions se trouvent au cœur de textes publiés à l’issue de la seconde guerre mondiale par Lazslo Moholy-Nagy et Frank-Lloyd Wright. Il n’est pas inutile de relire aujourd’hui ces textes et d’en apprécier la modernité. Ils sont au fondement d’une idée aujourd’hui très (trop ?) appelée, celle de design. Je m’efforcerai de préciser en quoi, et jusqu’à quel point.


    # Esthétiques de la communauté : Quelles formes peuvent prendre nos vies ?
    REMI ASTRUC

    Nous tenterons de prendre « forme » au sens fort et donc au sérieux, non comme une métaphore ou une façon de parler. Giorgio Agamben affirme en effet que les formesde-vie se définissent précisément par le fait que celles-ci ne peuvent être artificiellement séparées de leur forme. La prise en compte de la forme de la vie conduit alors naturellement à examiner cette dernière non pas aussi, mais peut-être prioritairement, dans la sphère de l’aisthesis, non pas pour l’évaluer esthétiquement (une vie plus ou moins belle que les autres) mais pour s’en saisir par la sensorialité (l’intensité affective qui la constitue). Être en commun, aspirer à la communauté, peut ainsi se dessiner de plusieurs manières, d’un trait ferme ou tremblant, brillant ou effacé, continu ou discontinu. Souvent, l’attention démesurée portée au « contenu » de ce qui serait commun a fait disparaître la figure que dessinent nos liens. Or les inquiétudes aujourd’hui sur la disparition du contenu commun de nos vies
    appellent sans doute à porter plus d’attention et à accorder plus d’importance aux formes que celui-ci peut prendre et qui font justement apparaître l’être-en-commun comme forme-de-vie. Nous réfléchirons à ces questions en nous appuyant notamment sur les images dessinées par des mythes et des œuvres littéraires.

    Rémi Astruc est professeur de littératures francophones et comparées à l’université Paris-Seine (Cergy-Pontoise). Il est en charge de l’axe « constructions identitaires » au sein du laboratoire AGORA (E.A. 7392) au sein duquel il organise le séminaire « Communauté/communautés » depuis 2013. Il s’intéresse aux esthétiques du commun et de la communauté, c’est-à-dire aux productions esthétiques des groupes et des communautés ainsi qu’aux formes esthétiques que peut prendre le commun. Sur ces questions, il est notamment l’auteur de Nous ? L’aspiration à la Communauté et les arts, Versailles, RKI Press, 2016, (postscriptum de Jean-Luc Nancy) et a dirigé l’ouvrage collectif La Communauté revisitée/ Community Redux, (RKI Press, 2016, avant-propos de Yves Citton). Il anime le site de la CCC (Communauté des Chercheurs sur la Communauté) qui est un réseau international de chercheurs sur ces thématiques et constitue une archive sur le sujet.


    # Common(s) Materials
    JOHN BINGHAM-HALL

    Why have ply and wood boarding come to be so expressive of grassroots political action? Why is glass understood as an avatar of transparent democracy? In this presentation I will explore the materiality of the commons, and also the common materials reproduced time and again in the construction of particular kinds of political space. To what extent does the manifesting of politics in common materials have to do with real properties of those materials, and to what extent are they doing symbolic work? In other words, are these materials politically usefulness be found at a physical, even atomic level, in the way they treat light and sound or can be cut and assembled, or is their participation in the construction of politics based on metaphorical relationships to ideas about those politics. Drawing on real political spaces and ideas submitted to Theatrum Mundi’s Designing Politics ideas challenges, I will aim to open up a critique of the role of materials in political with a view to widening the language of community design.


    # Imagining Users
    TOM CLARK

    If we can complicate the question of designing community, or perhaps its conditions, with the view that in order that it be instituted it must first be imagined, in what ways can we respond to the infrastructural politics of participation and digital technology that shape the concerns of this meeting? Moreover, if we are also considering the communities these might shape, how do we consider their constituents beyond the aesthetic and functional limits of the habitual figure of infrastructures, ‘the user’’— a figure more often than not mobilized to maintain certain kinds of infrastructure, and one with pre-determined and circumscribed agency? The field of art provides a useful starting point to these questions. Specifically art also comes under increasing pressure from policy conditions to both conform to common infrastructure and ideas of the function of cultural work in the broader economy. But does this collapsing of art into other disciplines neutralize the critical positions developed through art, or re-mobilize them? I will develop an infrastructural perspective to explore these questions through the work and context of Assemble, Theatrum Mundi, and Arte Util among others. This will help to model how infrastructure might depend on deploying as well as denying the status quo at the level of imaginary and of functionality; to speculate on this links the user and the platforms through which the user is realized and question the rubric of infrastructural provision and combination; to outline instead, how an contributive infrastructure might also offer a being-in-common not defined by the re-combination of separate users, but as the negotiation of existing dependencies, affects and territories.


    # In search for the collective designer reimagining a nordic noir participatory design tale in the age of surveillance capitalism
    PELLE EHN

    Through analysis of concrete design examples participative design strategies and tactics are explored under conditions of a welfare state (prototyping, expanding democracy to the workplace, 1985), neoliberal globalisation (infrastructuring, alternative networks and «friendly hacking», 2015), and contemporary conservative populist nationalism (what to do here is the new challenge, is democracy enough?). (Democratic) Design Thinging is suggested to replace (liberal) Design Thinking. Embracing Latour, but turning him on his head, as Marx did with Hegel. Maybe even turning to Hannah Arendt and her distinctions between labour, work and action in the Human Condition, not to delimit the political to her category of action, but to argue for design thinging and participatory design as flickering movements between the three, merging (private)bodies, (collaborative) making, and (public) activism in the making of and by publics and commons in the small and in the many.


    # Penser et accompagner le changement de paradigme décisionnel grâce au numérique
    CYRIL LAGE

    L’urgence qui pèse sur nos administrations et instances de décisions est protéiforme : sociale, écologique, institutionnelle... Les modes de gouvernance actuels, aujourd’hui remis en cause notamment par la crise des Gilets Jaunes, arrivent à un tournant et se voient contraints d’évoluer. Ce tournant est depuis des années pressenti, étudié et analysé par les théoriciens de l’Open Gov. Depuis des années, l’écosystème des civic tech s’attache de diverses manières à renverser le paradigme décisionnel, pour remettre les citoyen.ne.s, les salarié.e.s, au coeur des processus de fabrication de la décision, qu’elle soit politique ou stratégique. Pour que la décision devienne un commun. Cap Collectif, entreprise indépendante, agit dans ce but. Le web est selon Dominique Cardon un espace de mise en conversation, or, Cap Collectif ne met pas en conversation les internautes. La plateforme que nous avons développée vise à l’établissement d’un échange, d’une co-construction. Huit applications participatives rendent ceci possible. A la clé, un échange apaisé, à l’instar des espaces de discussion de conversation disponibles ailleurs sur la toile comme les espace de commentaires des divers site web de presse par exemple. Cyril Lage présentera la méthode de conception de la plateforme Cap collectif ainsi que l’accompagnement proposé aux décideurs et décideuses qui souhaitent engager des démarches de participation citoyenne. Au lendemain du Grand Débat National, la perspective d’une démocratie délibérative permanente est d’actualité. Or, si la direction à prendre semble a priori emporter l’adhésion et l’unanimité, Cyril Lage évoquera les résistances rencontrées régulièrement lors de la mise en oeuvre du changement. Il proposera enfin des pistes de réflexion pour penser la transition des modes de gouvernance conjointement au cap visé.


    # Faire communauté « par l’architecture » : les services Internet en pair-à-pair
    FRANCESCA MUSIANI

    Si le dialogue et le conflit entre architectures techniques, proposant des modèles différents d’organisation, de société, de travail, de marché, n’est pas un enjeu propre au numérique, il s’est rarement manifesté avec autant d’ampleur et force qu’avec l’avènement de l’Internet et de la massification de ses usages. L’architecture technique qui sous-tend l’Internet actuel – à la fois celle de l’Internet global, et celle des réseaux, systèmes, services qui le peuplent – n’est pas statique, pas plus qu’elle ne s’est imposée d’elle-même grâce à une supériorité technique intrinsèque. L’histoire du « réseau des réseaux » est celle d’une évolution constante, qui répond à une logique de normalisation de fait, liée aux modifications des usages, en particulier à leur massification, et à un ensemble de choix non seulement techniques mais économiques, politiques, sociaux. Tout comme l’architecture de l’Internet a fait l’objet de controverses par le passé, elle est actuellement soumise à de nombreuses tensions, tandis que l’on discute de ses futurs et que, après en avoir reconnu l’importance en tant que levier de développement et de contraintes économiques, on en reconnaît pleinement le statut de mécanisme de régulation politique.

     Le développement de services basés sur des architectures de réseau décentralisées, distribuées, P2P est depuis les débuts de l’Internet un de ses importants modes de communication et de gestion des contenus numériques. Dans ces systèmes, la responsabilité des échanges ou des communications se trouve aux marges ou à la périphérie du système, et l’ensemble des ressources du système ne se trouve pas dans un même    endroit physique, mais est réparti dans plusieurs machines. Toutefois, si le concept de décentralisation est en quelque sorte inscrit dans le principe même de l’Internet – notamment dans l’organisation de la circulation des flux – son urbanisme actuel n’intègre ce principe que de manière limitée. Structuré par une poignée de grandes plateformes, l’Internet d’aujourd’hui centralise des masses considérables de données dans certaines régions de l’Internet, alors même qu’elles ont vocation à être rediffusées aussi vite dans de multiples endroits d’un réseau pleinement globalisé.

     Le recours à des architectures de réseau décentralisées et à des formes d’organisation distribuées pour les services Internet est donc envisagé par nombre de projets, entreprises, services, comme voie « alternative » possible à cette centralisation. Les implications sont multiples, en termes de performance technique, mais aussi pour redéfinir des concepts tels que la sécurité et la privacy, reconfigurer les emplacements des données et des échanges, les frontières entre le réseau et l’usager, les outils à disposition de ce dernier. En somme, le P2P peut avoir un impact sur l’attribution, la reconnaissance et la modification de droits entre utilisateurs et fournisseurs des services de l’écosystème Internet. Recourir à des architectures décentralisées et à des formes d’organisation distribuées du réseau est donc une manière alternative d’aborder certaines questions critiques de sa gestion, dans une perspective d’efficacité, de sécurité et de « développement numérique durable ».


    # Le Community manager comme tisserand, ou comment tirer le fil des émotions aux communautés
    JULIEN PIERRE & CAMILLE ALLOING

    À travers l’affective computing, la technologie a apporté aux industries de biens et de services la capacité de lire et stimuler les émotions des consommateurs. En se centrant sur l’utilisateur, le design propose également de réenchanter le parcours-utilisateur. La rencontre de ces deux champs permet aux principales plateformes du web social de déployer des fonctionnalités avec lesquelles les usagers peuvent partager leurs réactions émotionnelles. Mais les émotions ne sont que des états passagers propres aux individus : si elles les mettent en mouvement, elles ne disent rien de la cause ou des effets dans la mesure où la technologie n’est pas – encore – capable de saisir et correctement interpréter le contexte d’émergence d’une réaction émotionnelle (qu’elle n’est pas plus capable d’interpréter convenablement). Après tout, que veut dire un like ?

    Nous repartons de la définition des affects établie par Spinoza pour envisager les stratégies des plateformes, et avec elles des marques et des agents qui y travaillent. Ainsi, les fonctionnalités proposées potentialisent les capacités affectives des usagers : capacité à affecter les autres, capacité à montrer dans quelle mesure une expérience nous a affecté. Le community manager est l’opérateur principal de ces stratégies affectives : il va employer les leviers fournis par les plateformes pour affecter sa « communauté de fans » au profit de la marque. Nous nous sommes intéressés plus particulièrement à l’emploi des emoji sur Twitter pour saisir ce qu’impliquait le travail affectif. En choisissant tel emoji pour illustrer son tweet, en répondant avec un , ou en étant interpellé par un , le CM est pris dans l’affectivité de la marque, de la plateforme et des membres de sa communauté. Notre analyse (22 entretiens, 545 répondants, 115000 tweets) nous permet de suivre comment l’emoji circule, depuis des consortiums et des industriels qui en valident l’existence et l’apparence, jusqu’à des situations d’énonciation en ligne, en passant par la possibilité de s’en servir pour du sentiment analysis et donc pour une certaine économie.
    
    Suivre à la trace les emoji permet ainsi d’identifier un « territoire numérique de marque » (Le Béchec & Alloing), terme plus adéquat selon nous pour conceptualiser les communautés. Mais cette méthode a aussi le mérite de rendre saillantes d’autres lignes d’action propres au CM. En considérant les emoji comme des affordances affectives (de la même manière que le sont les tableaux de bord, les guides pratiques et les briefs stratégiques), le CM effectue des choix communicationnels et organisationnels parmi une gamme d’actions. Notre recherche montre alors comment s’entremêlent des lignes entre la marque et sa communauté, entre le web et l’organisation, entre les interactants de la conversation en ligne, entre les expériences des uns et les émotions des autres, entre les affordances et les choix éthiques. Le community manager apparait alors comme le tisserand entrelaçant les lignes d’action qui sont autant de mailles du social.


    # Fragmenter la communauté
    CLÉO COLLOMB

    Cette proposition ne cherche qu’à cultiver une sensibilité à une certaine forme de communauté. Elle part d’une interrogation : comment sortir de façon non-triviale des rhétoriques marketing du renouveau de la communauté en ligne ou de l’intelligence des foules sur le web, sans pour autant se satisfaire des discours de l’aliénation de nos vies au coeur du capitalisme des plateformes ? L’idée de « communauté virtuelle » était très populaire dans les années 90, on pensait que la technologie permettrait de redéployer une socialité conviviale. Dans les années 2000, la rhétorique de l’intelligence collective et de la sagesse des foules l’emporte sur celle de la communauté, les TIC sont alors perçues comme offrant des possibilités d’expression aux masses ainsi que la coordination de leurs activités. Mais il ne reste pas grand chose de la communauté dans ces foules sages puisque les acteurs qui en coordonnent les activités ne font rien d’autre qu’agréger de manière automatisée, algorithmique les actions d’un très grand nombre d’individus postulés comme autonomes et techniquement maintenus en relation d’indépendance les uns par rapport aux autres. Autrement dit, l’idée de sagesse des foules repose sur un individualisme méthodologique incompatible avec l’expérience de la communauté : personne n’y sait que (ni comment) ses contributions participent à la production de quelque chose de commun.

    La foule sage n’est pas la communauté car elle dispense les individus de toute dimension collective de l’existence. Mais le design de la foule sage a saisi quelque chose du numérique : il est constitutif de modalités spécifiques de notre présence les uns aux autres. Goody a montré le rôle qu’a joué l’écriture dans la révolte des esclaves à Bahia : en faisant circuler des billets, les émeutiers ont pu planifier des incendies simultanés, fixer des rendez-vous silencieusement, organiser « un public plus large, plus distant et plus impersonnel » que ne le permet l’oralité. L’informatique pouvant être comprise comme une radicalisation de la dimension logistique de l’écriture, les TIC ont pour spécificité de permettre la constitution d’un « être-ensemble en très grand nombre ».

    Cet être-ensemble en très grand nombre permet de thématiser la sensibilité que je cherche à cultiver : faire que la communauté ne se totalise pas, sans pour autant en passer par l’atomisation. Le nombre fragmente la communauté, lui évitant l’absolutisation. Et il ne permet pas à l’individu d’exister, car lorsque nous sommes plusieurs, l’unité individuelle est toujours déjà affectée, exposée au commun qui la dépasse. L’être-ensemble en très grand nombre permet de mettre au travail une pensée non des identités, mais des appartenances. Cette pensée suppose une forme de désidentification, de devenir-quelconque, tel qu’on peut l’éprouver dans les émeutes ou au coeur d’un pogo dans un concert de hardcore.

    Il ne s’y agit pas d’espaces d’échange, de parole, ni nécessairement d’action, ce sont des espaces de présence, où les corps s’exposent les uns aux autres et où les sujets disparaissent. Fragmenter le projet comme totalité, exister côte-à-côte, à la lumière des autres, devenir-quelconque, porter un gilet anonyme, exposer une fragilité dans la nuit.


    # Capricious Characters of the Community: Let’s see how this plays out.
    JAMIE ALLEN, BERNHARD GARNICNIG & LUCIE KOLB

    The actions, activities and instincts of individuals that undertake knowledge practices within institutions co-constitute their potentials, politics and effects. The ways we act and interact with and within institutions is tinged with ever-present and unpredictable tonal qualities, limiting and affording particular modes of participation. It is instructive, even instrumental, to reflect our performed roles back upon themselves, evoking how they reverberate through our artificial means of satisfying the need we have to work together toward practical action.

    If, as Deleuze remarked, “what we call an instinct and what we call and institution essentially designate procedures of satisfaction”, then we need to enact the various means of these procedures in order to render them explicit — to see how they play out as was in which we institute “an original world between its tendencies and the external milieu”. We perform, and ask the collective co-created here, to enact and enable with us, the capricious characters of a conference that seeks to discuss and develop the conditions under which communities emerge. Through this enactment we hear how such contexts demand of us the construction of enacted roles, within the constraints and affordance of scenographic infrastructure. We do this as articulation of participation, and to resonate the tone of our capacity to live, work and make together.



    # Les « collectifs » d’architectes français. De la pratique à l’enquête de terrain : conclusions critiques.
    MATHIAS ROLLOT & ARTHUR POIRET

    Par la réflexion critique autant que par le biais d’exemples concrets, l’intervention propose quelques conclusions à la recherche nationale menée entre 2015 et 2018 et récemment parue sous le titre L’hypothèse collaborative. Conversation avec les collectifs d’architectes français (Hyperville, 2018). Ce qui constituera l’occasion de préciser les orientations communes, les différences et spécificités de parcours de ces acteurs regroupés sous l’appellation « collectifs ». Et donnera à lire non seulement l’importance de changer nos manières d’appréhender la commande, notre rapport à l’économie du projet, et la pluralité des compétences nécessaires à la réalisation du projet spatial et social ; mais aussi la nécessité d’une attention fine et prudence, si ce n’est critique, sur le sujet des « collectifs » et du lexique de la participation en architecture.



    # Art Eco-systems
    MICK FINCH

    The paper will look at how agendas such SteAm and ‘art inspired’ production have substituted art for design disciplines. However, the logic of ‘Industry 4.0’ was to genuinely look to how practices can disrupt conventional thinking in terms of methodologies such as open manufacturing. The key here seems to be how art mindsets tend to be ‘means orientated’ and able to identify specificities which are arguably a key quality for innovation.

    EU innovation funding streams and projects such as Fab Cities push the logic of how localized manufacturing and the rapidly closing gap between producer and consumer/client opens opportunities to configure how communities can be developed and sustained by engaging with these processes. Artistic practice, both in terms of ideas of production/ manufacture and in the relational sphere are crucial in thinking through such ideas. The paper will discuss these points in relation to projects currently being explored by the Art Programme at CSM in collaboration with its design departments and external design orientated organisations. The paper will look to how the shift in Industry 4.0 manufacturing processes and digital capture is recalibrating art practices and how it raises questions about what the studio will become, what is the reprographic and the multiple and what is the artist’s relationship to communities. A new art eco-system is emerging where collaboration, making and distribution is being transformed. However, fine art education tends to continue to focus on disciplinary boundaries with the gallery and museum systems as the dominant distributive culture. There is need for art practice to move closer to radical design to break this deadlock.


    # Fictionalising Commoning as Co-Design
    SHINTARO MIYAZAKI & MICHAELA BÜSSE

    Based on the results of a workshop we run a few weeks before the conference, we will reflect on the use of fiction in practice-based research processes. “Thinking Toys for Commoning” is a SNFS-funded project which playfully inquires approaches to model making as a means for grasping the complexities of co-living in shared spaces, using shared resources and infrastructures. Next to playing and modelling, the project investigates fictionalising (especially utopian narratives) as a strategy to deal with systems which defy control. If the future is unknown and the present full of uncertainties, how can we implement and what can we learn from utopian thinking? In order to explore visions on sharing and resource allocation we have created a workshop format that makes use of science fiction storytelling, prototyping (with artifacts, toys and other objects) and role play as methods. Selected pieces of utopian literature were analysed, disassembled into key elements and reassembled into alternative configurations by a group of participants. The newly crafted narrations will feed into the process of designing further toys for commoning. In our understanding, design acts as a diffractive technique, a moderator or facilitator between potentialities and their actualisation.



    # Participez ! Pour une critique mésologique du co-design
    LUDOVIC DUHEM

    Avec le développement actuel du design d’innovation sociale, du design de services, du design de politiques publiques, soutenu par les dispositifs numériques mis à disposition du public non expert en ligne et dans les « tiers lieux », les démarches de « co-design » apparaissent comme le nouveau paradigme de la « fabrique des communautés ». La participation s’impose alors comme la condition sine qua non du projet « communautaire » que le design propose aussi bien sur le plan social, économique que politique, pour construire une alternative au modèle dominant de la démocratie industrielle.

    Or, le fait de prendre part à quelque chose ne garantit pas nécessairement l’intégration ni l’émancipation du participant. À cet égard, la participation peut aussi se donner à voir sous d’autres figures, bien plus problématiques : injonction autoritaire, stratégie manipulatoire, idéologie mensongère, système de contrôle, etc. Il est donc nécessaire de revenir sur le concept de « participation » et sur la multiplicité de ces modalités pratiques afin de comprendre en quoi le co-design peut installer un dispositif de pouvoir paradoxalement instrumentalisant et coercitif alors que les intentions affichées sont à l’opposé.

    Plus généralement, c’est à une « mésopolitique » du co-design que sera consacrée cette intervention, en dialogue avec les philosophies de Simondon, Berque et Foucault, c’est-à-dire à une politique du « milieu » où la participation est autre chose qu’une nouvelle forme d’instrumentalisation des citoyens pour maintenir la paix sociale, à savoir ce qui révèle l’intrication réciproque et dynamique entre l’être humain et le milieu, qui est inséparablement éco-techno-symbolique. Le co-design serait en ce sens un design des communautés humaines et non-humaines, un soin individuel et collectif apporté aux milieux de vie.



    # Poor Sculpture: Making contingent markers with localized wireless networks
    ELIZABETH WRIGHT

    ‘Involuntary Works’ is a term first applied to a series of photographs made by the photographer Andre Brassaï in 1933 for the third and fourth editions of the Surrealist Magazine Minotaure. Everyday mundane objects such as bus tickets and bread rolls, photographed in extreme close up were monumentalized; becoming through the process of recording an ‘Involuntary sculpture’. Employing current analogue technology and through the print distribution system of a magazine publication, the previously established notions of sculpture as materially permanent markers that were singularly authored had been countered. Offline customizable wireless networks can be equally understood as providing an alternative platform for marking space. Mazi is a term used to describe a set of adaptable tools hosted on a localized platform. Its name, derived from the Greek word for ‘together’, communicates how Do-It-Yourself low-cost networking hardware, and free/libre/open source software ‘Floss’ applications can be adapted as a community tool to facilitate collective exchange and creative production. Reverso, a project developed across two different community contexts São Paulo, Brazil and London, UK will be used as a case study. Alternative approaches to the deployment of Mazi as a platform for distributed learning and hosting open-source software creative tools to facilitate community action will be discussed. How three-dimensional digital open-source modelling applications might be used to facilitate the recovery and activation of shared local histories in order to produce contingent digital markers will be introduced. The extent to which Mazi can be understood as offering new forms of digital sculptural fluidity through collective production and authorship will be considered.



    # Simondon, Lyotard and the transdisciplinary shadow
    MARTIN WESTWOOD

    Drawing from experiences as an artist involved in a cross-disciplinary project with
    archaeologists based in Saint-Denis, France (NEARCH – New Scenarios for a Community Based Archaeology), this presentation observes relationship and antagonism between holistic and discrete fields of knowledge, aesthetics, community, communication and noise. The beheaded body of the walking and talking cephalophore, St. Denis, is proposed as a postal worker – a gory cipher for the media delivery of messages. The talk describes the distribution of a series of questions (generated by consideration of Gilbert Simondon’s proposition of aesthetics as ecumenical thought, Jean Francois Lyotard’s notion of the differend and Michel Serres idea of the parasite) across the postal worker’s de-capitated body: To what extent does the formation of a universal community depend upon successful relations and practices of communication? If founding a community requires an act of exclusion (of noise, dispute, silence or remoteness) in order to successfully exchange messages, can architects of new communities acknowledge and incorporate this act of exclusion? Can the formal character of the exclusion be inverted into a building block for a new community where community might be made from “differences, noise and disorder” and “not in the key of pre-established harmony”? (Michel Serres, The Parasite (2007), Minnesota Press Edition. 13).


    # Renouer avec l’entreprise collective. La Chaire IDIS à l’épreuve du territoire.
    ÉMELINE EUDES

    Dans son livre Les trois écologies, Félix Guattari appelait déjà en 1989 à créer de nouveaux systèmes de valeurs, où la singularité – de l’individu, des projets collectifs, des relations à inaugurer avec nos milieux de vie – s’imposerait comme pilier, face à la standardisation des désirs et des imaginaires : « il est de moins en moins légitime que les rétributions financières et de prestige des activités humaines socialement reconnues ne soient régulées que par un marché fondé sur le profit. Bien d’autres systèmes de valeur seraient à prendre en compte (la «rentabilité» sociale, esthétique, les valeurs du désir, etc.). [...] la question se profile d’une mise à disposition des moyens de mener des entreprises individuelles et collectives allant dans le sens d’une écologie de la resingularisation.» Chercher à redonner de la valeur à la singularité des entreprises, notamment collectives, constitue ainsi l’objectif de fond du programme de la Chaire IDIS – Industrie, Design & Innovation Sociale. Hébergée à l’Ecole Supérieure d’Art et de Design de Reims, la Chaire IDIS est une plateforme créative permettant aux différents acteurs du territoire Grand Est de se rencontrer et de développer ensemble de nouveaux objets, de nouvelles méthodes de travail et de nouvelles histoires partagées. C’est en rencontrant un à un ces acteurs – PME/PMI, pôles de compétitivité, laboratoires de recherche, associations, centres de formation, habitants..., c’est en élaborant avec eux des projets de création, en les invitant à pérenniser ces liens entre nous tous, que des formes sociotechniques nouvelles émergent. Il ne s’agit donc plus de centraliser le ‘pouvoir’ de faire en un lieu, une usine, une entité, mais de l’inventer à travers un maillage hybride entre des savoir-faire et savoir-être différents, épars et pour certains, encore jamais reliés. Il s’agit d’« entreprendre autrement », comme l’écrit Bernard Stiegler, pour que l’entreprise humaine, trop longtemps délaissée au seul capitalisme industriel, participe désormais d’une écologie de la singularité. Or cette singularité s’exprime moins sur un plan personnel que par la notion de « projet », qui sous-entend pour l’équipe de la Chaire IDIS des formes « d’entreprises collectives ». Alors comment passe-t-on de l’ «entreprise commerciale» au « projet commun », du profit au sens? Comment fédère-t-on les énergies à travers le projet de design? Et comment évalue-t-on si l’entreprise collective est opérante pour ses acteurs? Voilà autant de questions que nous pratiquons au quotidien, vérifiant là le pouvoir d’invention et d’action du design.


    # Design By Belleville
    IGOR GALLIGO & JEANNE LACOUR

    Nous souhaitons repenser le design d’une économique politique des affects (Citton ; Lordon) qui tienne compte des caractéristiques et dynamiques culturelles, économiques et socio-techniques du territoire de Belleville afin de composer une communauté bellevilloise cosmopolitique. Si la politique consiste (aussi) dans un agencement
    des affects de la multitude et si la fabrique d’une politique post-globalisée nécessite la prise en considération d’une échelle territoriale, alors la politique bellevilloise doit articuler une étude des affects bellevillois avec un contexte économique globalisé, et des dispositifs socio-techniques qui en sont les instruments de médiation. Nous faisons l’hypothèse de l’existence de deux tendances socio-techniques opposées qui corrompent (Negri, Hardt) la formation d’une telle économie politique des affects. Une tendance à la fermeture et à l’entre-soi qui conduit à une appropriation communautariste de certaines parties du territoire, et une tendance à l’évasion par l’usage de technologies de télécommunication et de réseaux sociaux numériques qui désincarnent et dissipent les affects et les attentions. Quel design des affects faut-il donc réinventer pour dialectiser la localité d’une expérience incarnée du territoire et la possibilité d’une ouverture à une communauté cosmopolitique, qui peut tirer profit des organes socio-techniques des communautés bellevilloises en tant qu’instruments de médiation entre local et global ? C’est à cette problématique que notre présentation voudrait proposer quelques pistes de recherche

    "
  - title: Ressources
    content: "
      ![Affiche de l'evenement](/../../assets/media/events/002-poster-600.webp){.color loading=lazy}
      { .demi }

      - [Affiche de l'evenement](/../../assets/media/events/002-poster.pdf){.link .l}

      - [Programme de l'evenement](/../../assets/media/events/002-programme.pdf){.link .l}

      { .demi }
    "

#
# Programme
#
programme:

  # Jour 1
  - date: 2019-04-19
    title: Journée 1
    schedule:
      - session: Ouverture
        format: ouverture
        moderator: Igor Galligo
        start_time: 9:00
        end_time: 9:15

      - session: Commun, communauté et affects du nous
        title: Dire « nous » aujourd’hui
        description: 
        speakers: Marielle Macé
        start_time: 9:15
        end_time: 9:50

      - session: Commun, communauté et affects du nous
        title: Vivre l'époque
        speakers: Pierre-Damien Huyghe
        start_time: 9:50
        end_time: 10:25

      - session: Commun, communauté et affects du nous
        format: 
        title: Esthétiques de la communauté&thinsp;&#58; quelles formes peuvent prendre nos vies ?
        speakers: Remi Astruc
        start_time: 10:25
        end_time: 11:00

      - session: Commun, communauté et affects du nous
        format: Discussion
        start_time: 11:00
        end_time: 11:25

      ###
      
      - session: Matérialités et infrastructures du commun
        title: Common(s) materials
        speakers: John Bingham-Hall 
        start_time: 11:25
        end_time: 12:00

      - session: Matérialités et infrastructures du commun
        title: Imagining users
        speakers: Tom Clark
        start_time: 12:00
        end_time: 12:35

      - session: Matérialités et infrastructures du commun
        format: Discussion
        start_time: 12:35
        end_time: 12:55

      ###

      - break: true
        start_time: 12:55
        end_time: 14:25

      ###

      - session: Démocratie et design participatif &#58; quels nouveaux paradigmes ?
        title: In search for the collective designer - Reimagining a nordic noir participatory design   Tale in the age of surveillance capitalism
        description: 
        speakers: Pelle Ehn
        start_time: 14:25
        end_time: 15:00

      - session: Démocratie et design participatif &#58; quels nouveaux paradigmes ?
        format: Débat
        moderator:
          - Marion Waller 
        speakers: 
          - Richard Sennett
          - Bernard Stiegler
          - Tim Ingold
        start_time: 15:00
        end_time: 16:25

      - session: Démocratie et design participatif &#58; quels nouveaux paradigmes ?
        format: Débat
        speakers: 
          - Chris Younès
          - Patrick Bouchain
          - Federica Gatta
        start_time: 16:25
        end_time: 17:30

        ###

      - break: true
        start_time: 17:30
        end_time: 17:45   

        ###

      - session: L’avenir techno-politique des gilets jaunes
        title: Penser et accompagner le changement de paradigme décisionnel grâce au numérique
        description: 
        speakers: Cyril Lage
        start_time: 17:45
        end_time: 18:20  

      - session: L’avenir techno-politique des gilets jaunes
        format: Entretien
        moderator: Igor Galligo 
        speakers: Priscillia Ludosky & Co.
        start_time: 18:20
        end_time: 18:55

      - session: L’avenir techno-politique des gilets jaunes
        format: Discussion
        start_time: 18:55
        end_time: 19:30      


  # Jour 2
  - date: 2019-04-20
    title: Journée 2
    schedule:
      - session: Ouverture
        format: ouverture
        moderator: Igor Galligo
        description: 
        start_time: 9:00
        end_time: 9:10

      - session: La communauté numérique &#58; décentraliser, fragmenter, tisser
        title: Faire communauté « par l’architecture » &#58; les services internet en pair-à-pair
        speakers: Francesca Musiani
        description: 
        start_time: 9:15
        end_time: 9:45

      - session: La communauté numérique &#58; décentraliser, fragmenter, tisser
        title: Le community manager comme tisserand, ou comment tirer le fil des émotions aux communautés
        speakers: 
          - Camille Alloing
          - Julien Pierre
        description: 
        start_time: 9:45
        end_time: 10:20

      - session: La communauté numérique &#58; décentraliser, fragmenter, tisser
        format: 
        title: Fragmenter la communauté
        speakers: Cléo Collomb
        description: 
        start_time: 10:20
        end_time: 10:55

      - session: La communauté numérique &#58; décentraliser, fragmenter, tisser
        format: Discussion
        start_time: 10:55
        end_time: 11:20

      ### 

      - session: « Collectifs » versus institutions &#58; quels dénouements ?
        title: Capricious characters of the community &#58; let’s see how this plays out 
        speakers:
          - Jamie Allen 
          - Bernhard Garnicnig
          - Lucie Kolb
        start_time: 11:20
        end_time: 11:55

      - session: « Collectifs » versus institutions &#58; quels dénouements ?
        title: Les « collectifs » d’architectes français. de la pratique à l’enquête de terrain &#58; conclusions critiques 
        speakers:
          - Mathias Rollot
          - Arthur Poiret
        start_time: 11:55
        end_time: 12:30

      - session: « Collectifs » versus institutions &#58; quels dénouements ?
        format: Discussion
        start_time: 12:30
        end_time: 12:50

      ###

      - break: true
        start_time: 12:50
        end_time: 14:20

      ###

      - session: Co-design &#58; critiques et bifurcations
        title: Art eco-systems
        speakers: Mick Finch
        start_time: 14:20
        end_time: 14:55

      - session: Co-design &#58; critiques et bifurcations
        title: Fictionalising commoning as co-design
        speakers: 
          - Shintaro Miyazaki
          - Michaela Büsse
        start_time: 14:55
        end_time: 15:30

      - session: Co-design &#58; critiques et bifurcations
        title: Participez ! pour une critique mésologique du co-design
        speakers: Ludovic Duhem
        start_time: 15:30
        end_time: 16:05

      - session: Co-design &#58; critiques et bifurcations
        format: Discussion
        start_time: 16:05
        end_time: 16:30    

      ###

      - break: true
        start_time: 16:30
        end_time: 16:45   

      ###

      - session: Technologies de communication, territoire et fabrique de la communauté
        title: Poor sculpture &#58; making contingent markers with localized wireless networks
        speakers: Elizabeth Wright
        start_time: 16:45
        end_time: 17:20

      - session: Technologies de communication, territoire et fabrique de la communauté
        title: Simondon, Lyotard and the transdisciplinary shadow
        speakers: Martin Westwood
        start_time: 17:20 
        end_time: 17:55

      - session: Technologies de communication, territoire et fabrique de la communauté
        title: Renouer avec l’entreprise collective. la chaire idis à l’épreuve du territoire
        speakers: Émeline Eudes
        start_time: 17:55 
        end_time: 18:30

      - session: Technologies de communication, territoire et fabrique de la communauté
        title: Design by belleville
        speakers: 
          - Igor Galligo
          - Jeanne Lacour
        start_time: 18:30 
        end_time: 19:05

      - session: Technologies de communication, territoire et fabrique de la communauté
        format: Discussion
        start_time: 19:05
        end_time: 19:30

#
# Partenaires
#

partners:
  - name: École d'architecture de la ville & des territoires à Ma rne-la-vallé
  - name: Noödesign
  - name: Theatrum Mundi
  - name: Gerphau
  - name: Usbek & Rica
  - name: Institute of Experimental Design and Media Culture
  - name: EHESS
  - name: Desis network
  - name: APCI
  - name: Design en recherche
  - name: UAL Central Saint Martins
  - name: University of Applied Sciences Northwestern Switzerland






---

À une époque où l’urgence écologique et sociale fait peser plus que jamais la nécessité d’une nouvelle pensée et fabrique du commun, nous interrogerons le design sur sa capacité à contribuer à la fabrique de son organe culturel, la communauté, à partir de ses ressorts psychosomatiques, l’affect commun et le territoire. Si la recherche en design consiste notamment dans l’invention de nouveaux processus et circuits de production, l’enjeu de ces rencontres consistera à réfléchir à la production de notre être-en-commun à partir des infrastructures de production de notre milieu technique, et de leur capacité à tisser et vivifier des affects communs portés par des enjeux démocratiques, cosmopolitiques et esthétiques. Scientifiques, philosophes, artistes, designers, architectes et entrepreneurs français et européens sont ainsi invités pour réfléchir et formuler ensemble un nouvel âge numérique et participatif de la fabrique de la communauté, appelé de vive-voix par la crise politique exprimée par le mouvement français des Gilets Jaunes.
